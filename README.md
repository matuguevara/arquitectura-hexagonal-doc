# Cómo implementar la arquitectura hexagonal en frontend (Javascript/Typescript)

Tabla de contenidos:

1. [ Introducción ](#introduction)
2. [ ¿Por qué se llama hexagonal a esta arquitectura? ](#why)
3. [ Mismo concepto, distintos nombres ](#same)
4. [ ¿Cómo afecta a la mantenibilidad? ](#maintainability)
5. [ ¿Cómo afecta al frontend? ](#frontend)
6. [ Contexto histórico ](#historical)
7. [ Presente ](#present)
8. [ Consecuencias ](#consequences)
9. [ ¿Qué hacemos con los frameworks/librerias? ](#frameworks)
10. [ Ejemplo de arquitectura hexagonal ](#example)
    * [ Directories structure ](#examDir)
    * [ Domain ](#examDomain)
    * [ Data access ](#examData)
    * [ Views ](#examViews)
    * [ Third party libraries ](#examThird)
    * [ Tests ](#examTest)

<a name="introduction"></a>
## Introducción :wave:
Existen múltiples definiciones del término arquitectura, dependiendo del contexto y de la rama de desarrollo de la que se provenga. Por estas razones es complicado llegar a un consenso y a una definición única que sea válida para todos los casos. Así, según el desarrollo de software frontend, y desde un punto de vista profesional, la definición podría ser la siguiente:

**Los desarrolladores llaman arquitectura al conjunto de patrones de desarrollo que nos permiten definir las pautas a seguir en nuestro software en cuanto a límites y restricciones. Es la guía que debemos seguir para ordenar nuestro código y hacer que las distintas partes de la aplicación se comuniquen entre sí.**

Existe un amplio abanico de opciones a la hora de elegir una arquitectura u otra. Cada
tendrá sus propias ventajas e inconvenientes. Incluso una vez que elegimos cuál es la
más adecuada para nuestro caso, no tiene por qué implementarse de la misma forma en diferentes proyectos.

Sin embargo, aunque la cantidad de opciones es casi infinita, la mayoría mantienen en común sus atributos de calidad, tales como: escalabilidad, responsabilidad única, bajo acoplamiento, alta cohesión, etc.

Así que, de manera general, es crucial entender los conceptos, y la razón por la que se ha elegido una solución u otra.

Uno de los patrones más utilizados para diseñar la arquitectura de software es la Arquitectura Hexagonal, también conocida como Puertos y Adaptadores.

El objetivo de este patrón es dividir nuestra aplicación en diferentes capas, permitiendo que evolucione de forma aislada y haciendo que cada entidad sea responsable de una única funcionalidad.

<a name="why"></a>
## ¿Por qué se llama hexagonal a esta arquitectura? :thinking:

La idea de representar esta arquitectura con un hexágono se debe a la facilidad de asociar el concepto teórico con el visual.
Dentro de este hexágono es donde se encuentra nuestro código base. Esta parte se denomina **dominio**.

Cada lado de este hexágono representa una interacción con un servicio externo, por ejemplo: servicios http, db, rendering...

<img src="./resources/hexagonal_architecture.png" style="background-color: white" alt="hexagon" height=300 />


La comunicación entre el **dominio** y el resto de actores se realiza en la capa **infraestructura**. En esta capa implementamos un código específico para cada una de estas tecnologías.

Una de las preguntas más recurrentes entre los profesionales que ven esta arquitectura por primera vez es: ¿Por qué un hexágono? Bueno, el uso de un hexágono es sólo una representación teórica. El número de servicios que podríamos añadir es infinito, y podemos integrar tantos como necesitemos.

<a name="same"></a>
## Mismo concepto, distintos nombres :unamused:

El patrón de Arquitectura Hexagonal también se denomina **Puertos y Adaptadores**. Este nombre proviene de una separación dentro de una capa de **infraestructura**, donde tendremos dos subcapas:

- **Puerto**: Es la interfaz que nuestro código debe implementar para abstraerse de la tecnología. Aquí definimos las firmas de métodos que existirán.


- **Adaptador**: Es la implementación de la propia interfaz. Aquí tendremos nuestro código específico para consumir una tecnología concreta. Es importante saber que esta implementación NO debe estar en nuestra aplicación, más allá de la declaración, ya que su uso se realizará a través del **puerto**.

Así, nuestro dominio realizará llamadas a la subcapa que corresponda al puerto, desacoplándose de la tecnología, mientras que el puerto, a su vez, consumirá el adaptador.

El concepto de **Puertos y Adaptadores** está muy ligado a la programación orientada a objetos y al uso de interfaces, y quizás, la implementación de este patrón en programación funcional podría ser diferente al concepto inicial. De hecho, han surgido muchos patrones que iteran sobre este, como **Arquitectura de cebolla** o **Arquitectura limpia**. Al final el objetivo es el mismo: dividir nuestra aplicación en capas, separando **dominio** e **infraestructura**.

<a name="maintainability"></a>
## ¿Cómo afecta a la mantenibilidad? :hammer:

El hecho de tener nuestro código separado en capas, donde cada una de ellas tiene una única responsabilidad, ayuda a que cada capa pueda evolucionar de formas diferentes, sin impactar a las demás.

Además, con esta segmentación conseguimos una alta cohesión, donde cada capa tendrá una responsabilidad única y bien definida dentro del contexto de nuestro software.

<a name="frontend"></a>
## ¿Cómo afecta al frontend? :open_mouth:

Actualmente existen una serie de deficiencias en el uso de metodologías a la hora de crear aplicaciones. Hoy en día, disponemos de una cantidad increíble de herramientas que nos permiten desarrollar aplicaciones muy rápidamente y, al mismo tiempo, hemos dejado en un segundo plano el análisis y la implementación de arquitecturas conocidas y probadas.

No obstante, aunque estas arquitecturas puedan parecer del pasado, donde los lenguajes no evolucionaban tan rápido, estas arquitecturas han sido mostradas y adaptadas para darnos la escalabilidad que necesitamos para desarrollar aplicaciones reales.

<a name="historical"></a>
## Contexto histórico :sleeping:

Hace dos décadas, las aplicaciones de escritorio eran la principal herramienta de desarrollo. En ellas, toda nuestra aplicación se instalaba en la máquina, a través de librerías, y existía un alto acoplamiento entre vista y comportamiento.
Luego, quisimos escalar nuestras aplicaciones para conseguir un software más mantenible, con bases de datos centralizadas. Así que muchas de ellas se migraron a un servidor. Con esto, nuestras aplicaciones de escritorio se redujeron a aplicaciones "tontas", que no requerían acceso, persistencia o muchos datos.
Finalmente, si la app necesitaba algún dato, tenía la responsabilidad de realizar estas llamadas a los servidores externos a través de servicios de red. Es aquí cuando empezamos a distinguir entre "frontend" y "backend".

Durante los años siguientes se produjo el boom de la web. Muchas aplicaciones de escritorio se adaptaron a los navegadores, donde las limitaciones eran mayores con sólo HTML. Más tarde, JAVASCRIPT empezó a dar más posibilidades al navegador.

<a name="present"></a>
## Presente :relieved:

Las vistas siempre se habían limitado únicamente a la representación de datos y nunca habían necesitado funcionalidades superiores, hasta ahora. Con las necesidades comunes, las aplicaciones frontend tienen más requisitos que hace años. Por citar algunos ejemplos: gestión de estados, seguridad, asincronía, animaciones, integración con servicios de terceros...

Por todas estas razones, necesitamos empezar a aplicar patrones en estas aplicaciones.

<a name="consequences"></a>
## Consecuencias :worried:

Como hemos dicho, el propósito del frontend es principalmente visualizar datos. A pesar de esta percepción, NO es el **dominio** de nuestra aplicación, sino que pertenece a las capas externas de la arquitectura de la aplicación.

Los casos de uso de la aplicación sí pertenecen al **dominio**, y no deberían saber cómo deben visualizarse los datos.

Por ejemplo, supongamos que estamos desarrollando un carrito de la compra. Un caso de uso podría ser: "Un carrito de la compra no puede tener más de 10 productos".
Otro caso de uso: "Una cesta de la compra no puede tener el mismo producto dos o más veces". Podemos ver los casos de uso como requisitos en nuestra aplicación.

Las peticiones de datos al backend pertenecen a la capa **infraestructura**, y es algo que nuestra aplicación no necesita saber, aunque gestionemos el backend (es una aplicación diferente y tiene necesidades de arquitectura diferentes). En algún momento, el esquema de datos del backend podría cambiar, y no queremos que nuestra aplicación se vea afectada por ello.

Otra parte que pertenece a **infraestructura** es la gestión de datos locales, como datos de sesión, cookies o bases de datos locales. Por supuesto, tenemos que lidiar con esto, pero no es parte de nuestro **dominio**.

<a name="frameworks"></a>
## ¿Qué hacemos con los frameworks/librerias? :sunglasses:

Hoy en día hay una enorme cantidad de librerías para renderizar: Angular, React, Vue...; pero debemos entender cuál es su propósito, por lo que no deben entrar en el **dominio**, sino que deben manejarse en la **infraestructura**.

Todas estas herramientas tienden a evolucionar rápidamente, y nuestro código no quiere verse afectado por ellas. Ahora bien, ¿cómo podemos conseguirlo? Bueno, una de las estrategias más comunes es envolver estas librerías en funcionalidades creadas para este fin. Esta estrategia se conoce como "wrapping" y con ella conseguimos aislar nuestro código de los efectos secundarios que puedan tener estas librerías.

"Wrapping" es una buena práctica, pero cuando la utilicemos, debemos hacerlo a través de un **adaptador** para reducir el acoplamiento. Además, esta técnica también tiene desventajas. Por ejemplo, si abusamos de ella, podemos estar sobreingenierizando nuestro producto, aumentando el tiempo de mantenimiento. Por lo tanto, tenemos que identificar en qué casos merece la pena y en cuáles no.

Podemos afirmar que la comunicación entre la **vista (infraestructura)** y el **dominio** es unidireccional, es decir, son un punto de entrada para el usuario, pero nunca serán consumidos por el **dominio**.

Una vez entendido esto, tenemos que asumir que las herramientas altamente acopladas a estas librerías frontend, como Redux o Vuex, deben ser gestionadas en la capa de **infraestructura**.

<a name="example"></a>
## Ejemplo de arquitectura hexagonal :rocket:

Ahora es el momento del espectáculo, vamos a intentar poner en práctica toda esta teoría a través de un ejemplo. Escribamos algo de código.

Imaginemos que tenemos que diseñar un carrito de la compra, y tenemos que hacerlo en "reactjs", "vuejs" y "React Native".

Primero, pensamos en qué entidades entran en juego, sabiendo que recuperaremos los datos a través de un servicio de terceros (lo veremos más adelante).

- **Producto**
- **Carrito**

También sabemos que estas entidades deben estar disponibles para el usuario, para que éste pueda interactuar con ellas. El usuario podría hacer lo siguiente

- Ver una lista de productos
- Añadir productos a la cesta de la compra
- Eliminar productos de la cesta de la compra

Ahora imagine que tenemos las siguientes reglas de negocio:

- Un carro de la compra no puede tener más de 5 productos
- El mismo producto no puede estar en el carro dos veces o más
- El precio máximo del carro debe ser de 100

<a name="examDir"></a>
### Estructura de directorios :card_index_dividers:

Aquí podemos ver un ejemplo de cómo organizar los directorios, tanto para las aplicaciones "React" como "Vue".

<img src="./resources/directories.png" alt="Directories structure for hexagonal architecture" height=auto />

Voy a explicar un poco más lo que representa cada carpeta.

- **dominio**
    * **modelos** Aquí tenemos los modelos que vamos a necesitar, tanto tipos como interfaces de cada modelo.
    * **repositorios** Todos los tipos e interfaces relacionados con los repositorios (un repositorio se encarga de traer datos desde un servicio web, o una base de datos, o un fichero...).
    * **servicios** Un servicio se encarga de interactuar con nuestros modelos y realizar acciones sobre ellos. Por ejemplo para obtener los productos o añadir un producto al carrito.
- **Infraestructura**
    * **http** Aquí se almacenan cosas relacionadas con nuestro cliente, en este caso un cliente http.
        + **dto** Todos los dto's que recibimos de un repositorio.
    * **instances** Aquí hemos creado instancias concretas para nuestro cliente y repositorios. Puedes ver como el punto de entrada de tu sistema. Quizás este no sea el mejor lugar para esta carpeta, la hemos creado de esta forma para utilizar datos falsos ya que no disponemos de un servicio web.
    * **repositorios** Aquí se definen los repositorios que necesitamos para obtener productos.
    * **views** Esta carpeta almacena todo lo relacionado con nuestras vistas. 
        + **react-ui** Proyecto React que interactúa con nuestros modelos y servicios.
        + **reactnative-ui** Proyecto React Native que interactúa con nuestros modelos y servicios.
        + **vue-ui** Proyecto Vue que interactúa con nuestros modelos y servicios.
- **mocks** Aquí tenemos datos simulados que nuestro cliente utilizará para proporcionar dto's concretos.
- **test** Todos los test unitarios para los casos de uso.

Hemos creado dos directorios: domain e infrastructure. Todos los componentes visuales están dentro de infrastructure (recuerda que las vistas y representaciones no pertenecen a nuestro dominio).

<a name="examDomain"></a>
### Dominio :shield:

Ahora vamos a definir los modelos del dominio (Producto y Carrito) con las respectivas interfaces requeridas.

```ts
// src/domain/models/Product.ts

export type Product = {
    id: string;
    title: string;
    price: number;
};
```

```ts
// src/domain/models/Carts.ts

import { Product } from './Product';

// Esta interfaz define qué operaciones podemos realizar en un carro
export interface ICart {
    createCart: () => Cart;
    addProductToCart: (cart: Cart, product: Product) => Cart;
    removeProductFromCart: (cart: Cart, product: Product) => Cart;
}

export type Cart = {
    id: string;
    products: Product[];
};

```

Ahora definimos una funcionalidad que nos permita añadir y eliminar un "Producto", teniendo en cuenta los requisitos y reglas de negocio.

Dependiendo del patrón de diseño que utilicemos esta implementación puede ser ligeramente diferente. Para este caso usamos una opción fácil, un módulo de servicio que maneja los datos.

```ts
// src/domain/services/Cart.service.ts

import { Cart, ICart } from '../models/Cart';
import { Product } from '../models/Product';

const createCart = (): Cart => {
    return { id: Date.now().toString(), products: [] };
};

const hasProduct = (cart: Cart, product: Product): boolean => {
    return !!cart.products.find(item => item.id === product.id);
};

const isCartFull = (cart: Cart): boolean => {
    return cart.products.length >= 5;
};

const isCartLimitPriceExceeded = (cart: Cart, product: Product, limit: number): boolean => {
    let totalPriceCart = 0;
    cart.products.forEach(item => {
        totalPriceCart += item.price;
    });
    totalPriceCart += product.price;

    return totalPriceCart > limit;
};

const addProductToCart = (cart: Cart, product: Product): Cart => {
    if (!hasProduct(cart, product) && !isCartFull(cart) && !isCartLimitPriceExceeded(cart, product, 100))
        cart.products = [...cart.products, product];
    return { ...cart };
};

const removeProductFromCart = (cart: Cart, product: Product): Cart => {
    const productsWithRemovedItem: Product[] = [];
    cart.products.forEach(item => {
        if (item.id !== product.id) productsWithRemovedItem.push(item);
    });
    cart.products = [...productsWithRemovedItem];
    return { ...cart };
};

// Este servicio debe implementar las operaciones definidas para la interfaz Cart
export const cartService: ICart = {
    createCart,
    addProductToCart,
    removeProductFromCart
};

```

<a name="examData"></a>
### Acceso a los datos :newspaper:

Además, necesitamos obtener una lista de productos. En la mayoría de los casos estos datos se obtienen de servicios http, pero también podríamos utilizar graphql o cualquier otra librería. Además, dentro de http podríamos usar fetch, axios, xhr...

En cualquier caso, esto forma parte de la capa de infraestructura, y este objeto será consumido por una entidad del repositorio.

En primer lugar, definimos la estructura de los datos devueltos por la API. Este tipo de datos se denomina "Objeto de Transferencia de Datos (DTO)":

```ts
// src/infrastructure/http/dto/ProductDTO.ts

export interface ProductDTO {
    id: string;
    title: string;
    description: string;
    price: number;
}

```

Además, hemos declarado qué métodos necesitamos implementar para http. Así, más adelante podremos usar nuestro cliente favorito (fetch, axios...) implementando esta interfaz:

```ts
// src/domain/repositories/Http.ts

export interface Http {
    get: <T>(path: string, params?: Record<string, any>, config?: any) => Promise<T | any>;
    post: <T>(path: string, params?: Record<string, any>, config?: any) => Promise<T | any>;
    put: <T>(path: string, params?: Record<string, any>, config?: any) => Promise<T | any>;
    delete: <T>(path: string, params?: any, config?: any) => Promise<T | any>;
}

```

Nuestro cliente, que será una instancia que implemente la interfaz `Http`, será inyectado como dependencia a nuestro repositorio. Así, en cualquier momento, podremos cambiar nuestro cliente instantáneamente. Esta técnica se llama inyección de dependencias, y aunque no es muy común en javascript, es muy potente y está ahí para ser utilizada. Typescript nos lo pone muy fácil.

Para este ejemplo hemos creado dos clientes (wrappers), uno usará axios, y el otro devolverá un mock data.

#### Cliente para axios
```ts
// src/infrastructure/instances/httpAxios.ts

import axios from 'axios';
import { Http } from '../../domain/repositories/Http';

const headers = {
    'Content-Type': 'application/json'
};

export const httpAxios: Http = {
    get: async <T>(path: string, params?: Record<string, any>, config?: any) => {
        const response = await axios.get(path, { ...config, params: params, headers });
        return response.data as T;
    },
    post: async <T>(path: string, params?: Record<string, any>, config?: any) => {
        const response = await axios.post(path, { ...params }, { ...config, headers });
        return response.data as T;
    },
    put: async <T>(path: string, params?: Record<string, any>, config?: any) => {
        const response = await axios.put(path, { ...params }, { ...config, headers });
        return response.data as T;
    },
    delete: async <T>(path: string, params?: any, config?: any) => {
        const response = await axios.delete(path, { ...config, params: params, headers });
        return response.data as T;
    }
};
```

#### Cliente con falsa data
```ts
// src/infrastructure/instances/httpAxios.ts

import { Http } from '../../domain/repositories/Http';
import { productListMock } from '../../mocks/products';

export const httpFake: Http = {
    get: async <T>(path: string, params?: Record<string, any>, config?: any) => {
        const response = await productListMock;
        return response;
    },
    post: async <T>(path: string, params?: Record<string, any>, config?: any) => {
        const response = await productListMock;
        return response;
    },
    put: async <T>(path: string, params?: Record<string, any>, config?: any) => {},
    delete: async <T>(path: string, params?: any, config?: any) => {}
};
```

De esta forma, cuando quieras cambiar el cliente y usar, por ejemplo, fetch en lugar de axios, puedes crear un nuevo http wrapper que implemente la interfaz para http, pero usando la librería fetch. Muy fácil.

Para finalizar esta parte, necesitamos una última cosa. Tenemos que crear un repositorio de productos dentro de la infraestructura. Este repositorio manejar la solicitud y, la transformación de los datos de respuesta a nuestro modelo de dominio.

```ts
// src/infrastructure/repositories/productRepository.ts

import { Product } from '../../domain/models/Product';
import { ProductRepository } from '../../domain/repositories/ProductRepository';
import { Http } from '../../domain/repositories/Http';
import { ProductDTO } from '../../infrastructure/http/dto/ProductDTO';

export const productRepository = (client: Http): ProductRepository => ({
    getProducts: async () => {
        const products = await client.get<ProductDTO>('');
        return products.map((productDto): Product => ({ id: productDto.id, title: productDto.title, price: productDto.price }));
    },

    getProductsById: async id => {
        const products = await client.get<ProductDTO>('', { id });
        return products.map((productDto): Product => ({ id: productDto.id, title: productDto.title, price: productDto.price }));
    }
});
```

Como puedes ver, `productRepository` es una función que recibe un cliente como parámetro (justo aquí está la inyección de dependencia).

Por conveniencia, hemos creado un repositorio falso que implementa la interfaz `ProductRepository`.

**Recuerda que el código de producción no debe contener ninguna referencia a datos falsos o simulados. Lo estamos utilizando para hacer funcional este proyecto. Por esta razón, en producción debes olvidar la carpeta `instances`.**


<a name="examViews"></a>
### Vistas :iphone: :computer:

La vista y la capa de acceso a los datos están en la infraestructura. Sin embargo, no deben comunicarse directamente. Vamos a crear un nuevo servicio para consumir nuestro repositorio, por lo que estos datos estarán disponibles para el resto de nuestra aplicación.

```ts
// src/domain/services/ProductService.ts

import { ProductRepository } from '../repositories/ProductRepository';

// Aquí podemos cambiar el repositorio por uno que implemente la interfaz IProductRepository
//const repository: IProductRepository = productRepository;

export const productService = (repository: ProductRepository): ProductRepository => ({
    getProducts: () => {
        return repository.getProducts();
    },
    getProductsById: id => {
        return repository.getProductsById(id);
    }
});
```

Al igual que con nuestro cliente `Http`, estamos utilizando inyección de dependencias en nuestro servicio, que recibe un repositorio como parámetro. De esta forma, podremos cambiar el repositorio en cualquier momento. (Tal vez en el futuro, tengamos que obtener los productos de una base de datos local en lugar de un servicio rest). 

---

Bien, ahora hemos definido como obtener los datos, y la funcionalidad que necesitamos para añadir/eliminar elementos en nuestro carrito.

Independientemente de si usas react o vue, ten en cuenta que el código escrito hasta ahora es común a ambas aplicaciones, por lo que la llamada a nuestros métodos desde nuestro componente será la misma.

Dentro de la carpeta views hemos creado tantos proyectos como hemos necesitado. En nuestro caso tenemos react, vue y react native.


Lo primero que vamos a hacer saber es definir el estado inicial y, las funciones para manejar el estado del carrito.

#### React
```tsx
// src/infrastructure/views/react-ui/src/App.tsx

import React, { useState } from 'react';
import { ProductList } from './views/ProductList';
import { Cart } from '@domain/models/Cart';
import { Product } from '@domain/models/Product';
import { cartService } from '@domain/services/CartService';

const App = () => {
    const [cart, setCart] = useState<Cart>(cartService.createCart());

    const handleAddToCart = (product: Product) => {
        setCart(cartService.addProductToCart(cart, product));
    };

    const handleRemoveToCart = (product: Product) => {
        setCart(cartService.removeProductFromCart(cart, product));
    };

    const renderCartProducts = (): JSX.Element[] => {
        const cartProducts: JSX.Element[] = [];
        let totalCart = 0;

        cart.products.forEach(product => {
            totalCart += product.price;
            cartProducts.push(
                <div key={product.id}>
                    <label>{product.title} </label>
                    <span>({product.price} €) </span>
                    <button onClick={() => handleRemoveToCart(product)}>remove</button>
                    <br />
                </div>
            );
        });

        cartProducts.push(
            <div key={'total'}>
                <br />
                <label>
                    <b>Total:</b>
                </label>
                <span>{totalCart} €</span>
                <br />
            </div>
        );
        return cartProducts;
    };

    return (
        <div>
            <h1>Shopping cart</h1>
            <h2>Products in the cart</h2>
            {renderCartProducts()}
            <ProductList onSelectProduct={handleAddToCart} />
        </div>
    );
};

export default App;
```

#### Vue
```vue
<!--
// src/infrastructure/views/vue-ui/src/App.vue
-->
<template>
    <div id="app">
        <h1>Shopping cart</h1>
        <h2>Products in the cart</h2>
        <div v-for="product in cart.products" :key="product.id">
            <label>{{ product.title }} </label>
            <span>({{ product.price }} €) </span>
            <button @click="handleRemoveProductFromCart(product)">remove</button>
            <br />
        </div>
        <div>
            <br />
            <label>
            <b>Total:</b>
            </label>
            <span>{{ getTotalCart() }} €</span>
            <br />
        </div>
        <ProductList @onSelectProduct="handleAddProductToCart" />
    </div>
</template>

<script lang="ts">
import { Product } from '@/domain/models/Product';
import ProductList from '@/infrastructure/views/ProductList.vue';
import { cartService } from '@/domain/services/Cart.service';
import { Cart } from '@/domain/models/Cart';

type DataProps = {
    cart: Cart;
};

export default {
    components: {
        ProductList
    },
    data(): DataProps {
        return {
            cart: cartService.createCart()
        };
    },
    mounted() {
        this.cart = cartService.createCart();
    },
    methods: {
        handleAddProductToCart(product: Product) {
            this.cart = cartService.addProductToCart(this.cart, product);
        },
        handleRemoveProductFromCart(product: Product) {
            this.cart = cartService.removeProductFromCart(this.cart, product);
        },
        getTotalCart() {
            let totalCart = 0;
            this.cart.products.forEach(product => {
                totalCart += product.price;
            });
            return totalCart;
        }
    }
};
</script>
```

#### React Native
```tsx
// src/infrastructure/views/reactnative-ui/App.tsx

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, Button } from 'react-native';

import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Cart } from '@domain/models/Cart';
import { cartService } from '@domain/services/CartService';
import { Product } from '@domain/models/Product';
import { ProductList } from '@/components/ProductList';

const App = () => {
    const [cart, setCart] = useState<Cart>(cartService.createCart());

    const handleAddToCart = (product: Product) => {
        setCart(cartService.addProductToCart(cart, product));
    };

    const handleRemoveToCart = (product: Product) => {
        setCart(cartService.removeProductFromCart(cart, product));
    };

    const renderCartProducts = (): JSX.Element[] => {
        const cartProducts: JSX.Element[] = [];
        let totalCart = 0;

        cart.products.forEach(product => {
            totalCart += product.price;
            cartProducts.push(
                <View style={styles.productInCart} key={product.id}>
                    <Text>{product.title} </Text>
                    <Text>({product.price} €) </Text>
                    <Button color={'red'} onPress={() => handleRemoveToCart(product)} title={'remove'} />
                </View>
            );
        });

        cartProducts.push(
            <View style={styles.productInCart} key={'total'}>
                <Text>Total:</Text>
                <Text> {totalCart} €</Text>
            </View>
        );
        return cartProducts;
    };

    return (
        <>
            <StatusBar barStyle='default' />
            <SafeAreaView>
                <ScrollView contentInsetAdjustmentBehavior='automatic' style={styles.scrollView}>
                    <Text style={styles.titlePage}>Shopping cart</Text>
                    <Text style={styles.title}>Products in the car</Text>
                    {renderCartProducts()}
                    <ProductList onSelectProduct={handleAddToCart} />
                </ScrollView>
            </SafeAreaView>
        </>
    );
};

const styles = StyleSheet.create({
    scrollView: {
        backgroundColor: Colors.lighter
    },
    titlePage: {
        fontWeight: 'bold',
        margin: 5,
        fontSize: 20
    },
    title: {
        fontWeight: 'bold',
        margin: 5
    },
    productInCart: {
        flexDirection: 'row',
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        margin: 10
    }
});

export default App;

```

Vamos a mostrar la lista de productos.

#### React

```tsx
// src/infrastructure/views/react-ui/src/views/ProductList.tsx

import React, { useCallback } from 'react';
import { Product } from '@domain/models/Product';
import { productService } from '@domain/services/ProductService';
import { productRepositoryFake } from '@infrastructure/instances/productRepositoryFake';

interface ProductListProps {
    onSelectProduct: (product: Product) => void;
}

export const ProductList: React.FC<ProductListProps> = ({ onSelectProduct }) => {
    const [products, setProducts] = React.useState<Product[]>([]);

    const getProducts = useCallback(async () => {
        try {
            const responseProducts = await productService(productRepositoryFake).getProducts();
            setProducts(responseProducts);
        } catch (exception) {
            console.error(exception);
        }
    }, []);

    React.useEffect(() => {
        getProducts();
    }, []);

    const handleSelectProduct = (product: Product) => {
        onSelectProduct(product);
    };

    return (
        <div>
            <h2>List of products</h2>
            <ul>
                {products.map(product => (
                    <li key={product.id}>
                        <button
                            onClick={() => {
                                handleSelectProduct(product);
                            }}
                        >
                            {product.title}
                        </button>
                    </li>
                ))}
            </ul>
        </div>
    );
};
```

#### Vue
```vue
<!--
// src/infrastructure/views/vue-ui/src/views/ProductList.vue
-->
<template>
    <div>
        <h2>List of products</h2>
        <ul>
            <li v-for="product in products" :key="product.id">
                <button @click="handleSelectProduct(product)">{{ product.title }}</button>
            </li>
        </ul>
    </div>
</template>

<script lang="ts">
import { productService } from '@domain/services/ProductService';
import { Product } from '@domain/models/Product';
import { productRepositoryFake } from '@infrastructure/instances/productRepositoryFake';

type DataProps = {
    products: Product[];
};

export default {
    name: 'ProductList',
    data(): DataProps {
        return {
            products: []
        };
    },
    mounted() {
        productService(productRepositoryFake)
            .getProducts()
            .then(response => (this.products = response));
    },
    methods: {
        handleSelectProduct(product: Product) {
          this.$emit('onSelectProduct', product);
        }
    }
};
</script>
```

#### React Native
```tsx
// src/infrastructure/views/reactnative-ui/src/components/ProductList.tsx

import React, { useCallback, useState } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

import { Product } from '@domain/models/Product';
import { productService } from '@domain/services/ProductService';
import { productRepositoryFake } from '@infrastructure/instances/productRepositoryFake';

interface ProductListProps {
    onSelectProduct: (product: Product) => void;
}

export const ProductList: React.FC<ProductListProps> = ({ onSelectProduct }) => {
    const [products, setProducts] = useState<Product[]>([]);

    const getProducts = useCallback(async () => {
        try {
            const responseProducts = await productService(productRepositoryFake).getProducts();
            setProducts(responseProducts);
        } catch (exception) {
            console.error(exception);
        }
    }, []);

    React.useEffect(() => {
        getProducts();
    }, []);

    const handleSelectProduct = (product: Product) => {
        onSelectProduct(product);
    };

    return (
        <View>
            <Text style={styles.title}>List of products</Text>
            <View>
                {products.map(product => (
                    <View style={styles.buttonProduct} key={product.id}>
                        <Button
                            onPress={() => {
                                handleSelectProduct(product);
                            }}
                            title={product.title}
                        >
                            <Text>{product.title}</Text>
                        </Button>
                    </View>
                ))}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    title: {
        fontWeight: 'bold',
        margin: 5
    },
    buttonProduct: {
        margin: 5
    }
});
```

La gestión del estado de los elementos del carrito es interesante, pero es algo relacionado con la visualización de datos y esta gestión pertenece a la tecnología que estemos utilizando (React o Vue).

Además, si prestamos atención al código anterior, podemos ver que todo nuestro código está desacoplado. Nuestra capa de dominio puede ser utilizada por react, vue y react native.

Además, aquí podemos ver la inyección de dependencia en acción.
Utilizamos el servicio de producto, que recibe un repositorio específico `ProductRepository` que, a su vez, recibe un cliente específico `Http`.

<a name="examThird"></a>
### Librerias de Terceros :hammer_and_wrench:

Vamos a hacer un repaso rápido sobre una librería que hemos utilizado, en este caso axios. Nuestro **dominio** no debe saber nada de la existencia de esta librería, es decir, deberíamos poder cambiar de librería sin afectar a nuestro **dominio**.
Voy a explicarlo paso a paso:

- Hemos definido una interfaz `ProductRepository` que declara qué métodos deben ser implementados por nuestro repositorio.


- Hemos definido una interfaz `Http` que declara qué métodos deben ser implementados por nuestro cliente (un servicio web en este caso).


- Hemos instanciado un `productRepository` que implementa la interfaz `ProductRepository`.


- Hemos instanciado un `httpAxios` que implementa la interfaz `Http`.


- Nuestra instancia de `productRepository` utiliza una instancia de `httpAxios`. Si en algún momento quieres cambiar la librería http, por ejemplo, a fetch, sólo tienes que escribir otro cliente que implemente la interfaz `Http` y pasárselo al repositorio.


- Incluso si en algún momento quieres cambiar el repositorio, por ejemplo a una base de datos local, podrías escribir otro repositorio que implemente la interfaz `ProductRepository`.


Haciendo eso, has aplicado el principio de inversión de dependencias, así que ahora, tienes un código poco acoplado, y con alta cohesión.

<a name="examTest"></a>
### Tests :heavy_check_mark:

Por supuesto, algo que no puede perderse son las pruebas. Las pruebas son importantes, incluso cruciales. Sí, lo sé, quizás estés pensando... ¿Para qué voy a querer escribir pruebas si puedo probar el sistema manualmente?.
Bueno, es muy común que tu aplicación crezca con el tiempo, así que seguro que tendrás que cambiar algunas partes de tu aplicación. Entonces se vuelve muy tedioso volver a probar todo manualmente. Escribir tests es la mejor manera de estar seguros de que nuestro sistema funciona, y también, si algunos cambios futuros han afectado o roto nuestra aplicación.

Tenlo en cuenta, mucha gente tiene un porcentaje de cobertura de pruebas por encima del cual lo considera suficiente. ¿Qué opinas tú? ¿Quizás un 80% es correcto? Pues entonces tienes un 20% de tu sistema que puede fallar. No hay excusa para hacer las cosas bien, el único porcentaje de cobertura de pruebas que nos asegura que nuestra aplicación funciona como queremos es el 100%.

Aunque tengas un 100% de cobertura de código, eso no te asegura que estés cubriendo todos los casos.
Aquí un pequeño ejemplo:

`if (a > 1) ? 'Hi' : 'Bye'`

En este caso puedes hacer una prueba que compruebe que devuelve `Hi`, pero si no pruebas la devolución de `Bye` tienes un 100% de cobertura, pero tu código no está completamente probado.
**Así que, por favor, presta atención a todos los casos e intenta hacer pruebas inteligentes.**

Sí, yo era uno de ellos. He escrito código sin pruebas, pero no porque no quisiera hacer pruebas, el problema era que al tener todo el código disperso y mezclado con las vistas, era difícil si no imposible escribir pruebas.

Así que, vamos a escribir algunas pruebas. Ya veréis lo fácil que es escribir tests después de haber implementado una arquitectura adecuada...teniendo el **dominio** separado de la **infraestructura**.

Para hacer pruebas usamos `jest`. Desde mi punto de vista es la librería definitiva para hacer pruebas en javascript.

Primero vamos a tomar las reglas de negocio (descritas en la introducción de este ejemplo, ver arriba), y vamos a probar cada una de ellas.

```ts
// src/tests/cart.test.ts

import { cartService } from '../domain/services/CartService';
import { Product } from '../domain/models/Product';

const anyProduct = (id: string, price: number): Product => ({
    id,
    title: 'Any title',
    price
});

test('A car can not contain more than 5 products', async () => {
    const cart = cartService.createCart();

    cartService.addProductToCart(cart, anyProduct('1', 0));
    cartService.addProductToCart(cart, anyProduct('2', 0));
    cartService.addProductToCart(cart, anyProduct('3', 0));
    cartService.addProductToCart(cart, anyProduct('4', 0));
    cartService.addProductToCart(cart, anyProduct('5', 0));
    cartService.addProductToCart(cart, anyProduct('6', 0));
    expect(cart.products.length).toEqual(5);
});

test('If I add a product and it already exist in the cart, the product will not be added', async () => {
    const cart = cartService.createCart();

    cartService.addProductToCart(cart, anyProduct('1', 0));
    cartService.addProductToCart(cart, anyProduct('1', 0));
    expect(cart.products.length).toEqual(1);
});

test('If I add a product and it will exceed 100€, the product will not be added', async () => {
    const cart = cartService.createCart();

    cartService.addProductToCart(cart, anyProduct('1', 50));
    cartService.addProductToCart(cart, anyProduct('2', 60));
    expect(cart.products.length).toEqual(1);
});

```

## Esto es un ejemplo con test unitarios, la idea a futuro es armar un proyecto con test automatizados.

<a name="finalWords"></a>
## Conclusión :hugging:

Como hemos visto, implementar una buena arquitectura nos permitirá mejorar el mantenimiento del código. Además, estaremos desacoplados del framework/libreria que estemos utilizando, aportando más valor al **dominio**.

No olvides que este patrón se puede combinar con otros conceptos, como DDD, Programación Funcional...
No obstante, el objetivo de este ejemplo es dar una idea básica de cómo podríamos implementar la arquitectura hexagonal en un proyecto frontend.

<a name="bonus"></a>
## Bonus :heart:

- No me gustan las rutas absolutas, hacen que el código sea difícil de seguir. Así que este proyecto está preparado para usar rutas relativas usando `@alias` en `tsconfig.json` y `babel-plugin-module-resolver`.

- Código formateado con `eslint` y `prettier`.

- Para ejecutar el proyecto react ve a `src/infrastructure/views/react-ui/` y ejecuta `npm install && npm run start`.

- Para ejecutar el proyecto vue ve a `src/infrastructure/views/vue-ui/` y ejecuta `npm install && npm run serve`.

- Para ejecutar el proyecto react native ve a `src/infrastructure/views/reactnative-ui/` y ejecuta `npm install && npm run start`. A continuación, ejecute `npm run android`.